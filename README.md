# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação - Introdução a Engenharia. 
Da UTFPR-Pb
Consiste em: Desenvolver um site (página html) apresentando um dos experimentos realizados durante os cursos de e Arduino. 

|Nome| gitlab user|
|---|---|
|Guilherme|@GuilhermeRds19|
|Gladson|@Gladsonandrade|
|Fernando|@gudoskifer|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://guilhermerds19.gitlab.io/projetoarduino/ 

# Links Úteis

* [Tutorial HTML](http://pt-br.html.net/tutorials/html/)
* [Gnuplot](http://fiscomp.if.ufrgs.br/index.php/Gnuplot)